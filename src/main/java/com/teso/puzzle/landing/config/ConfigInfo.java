package com.teso.puzzle.landing.config;

import com.teso.framework.common.Config;

public class ConfigInfo {
    public static final String SPRING_BOOT_CONFIG = Config.getParam("spring_boot", "conf_path");
    public static final String STATIC_SERVER = Config.getParam("static_server","root_url");
    public static final String API_URL= Config.getParam("api_url","url");
}
