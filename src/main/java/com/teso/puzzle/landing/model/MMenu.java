package com.teso.puzzle.landing.model;

import java.util.List;

public class MMenu {
    private String id;
    private String name;
    private String thumbnail;
    private String description;
    private List< MProject> projects;

    public MMenu() {
    }

    public MMenu(String id, String name, String thumbnail, String description, List<MProject> projects) {
        this.id = id;
        this.name = name;
        this.thumbnail = thumbnail;
        this.description = description;
        this.projects = projects;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<MProject> getProjects() {
        return projects;
    }

    public void setProjects(List<MProject> projects) {
        this.projects = projects;
    }
}
