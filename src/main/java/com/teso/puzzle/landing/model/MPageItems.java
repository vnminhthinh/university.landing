package com.teso.puzzle.landing.model;

import java.util.List;

public class MPageItems {
   private String totalPages;
   private String page;
   private String totalCount;
   private String count;
   private List<MItem> items;

    public MPageItems() {
    }

    public MPageItems(String totalPages, String page, String totalCount, String count, List<MItem> items) {
        this.totalPages = totalPages;
        this.page = page;
        this.totalCount = totalCount;
        this.count = count;
        this.items = items;
    }

    public String getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(String totalPages) {
        this.totalPages = totalPages;
    }

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }

    public String getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(String totalCount) {
        this.totalCount = totalCount;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public List<MItem> getItems() {
        return items;
    }

    public void setItems(List<MItem> items) {
        this.items = items;
    }
}
