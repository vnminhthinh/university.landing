package com.teso.puzzle.landing.api;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.google.gson.reflect.TypeToken;
import com.teso.framework.utils.EmailUtils;
import com.teso.framework.utils.JSONUtil;
import com.teso.framework.utils.NetworkUtils;
import com.teso.puzzle.landing.config.ConfigInfo;
import com.teso.puzzle.landing.db.ItemRepo;
import com.teso.puzzle.landing.ent.Item;


import java.awt.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.*;
import java.util.*;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.teso.puzzle.landing.model.MItem;
import com.teso.puzzle.landing.model.MMenu;
import com.teso.puzzle.landing.model.MPageItems;
import jdk.nashorn.internal.objects.NativeError;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import sun.net.www.protocol.mailto.MailToURLConnection;

@Controller
public class MyController {

    private String apiUrl = ConfigInfo.API_URL;
    @RequestMapping("/")
    public String home(Model model, HttpServletRequest request) {
        String responseMenu = NetworkUtils.getResponse(apiUrl + "menus");
        Type list = new TypeToken<List<MMenu>>() {
        }.getType();
        List<MMenu> listMenu = JSONUtil.DeSerialize(responseMenu, list);
        String response = NetworkUtils.getResponse(apiUrl + "newitems");
        Type listType = new TypeToken<List<MItem>>() {
        }.getType();
        List<MItem> listNew = JSONUtil.DeSerialize(response, listType);
        model.addAttribute("static_url", ConfigInfo.STATIC_SERVER);
        model.addAttribute("itemList", listNew);
        model.addAttribute("menuList", listMenu);
        return "index";
    }

    @RequestMapping("/items")
    public String contacts(Model model, @RequestParam(name = "id", defaultValue = "0") String id,
                           @RequestParam(name = "page", defaultValue = "1") int page) {
        String responseMenu = NetworkUtils.getResponse(apiUrl + "menus");
        Type list = new TypeToken<List<MMenu>>() {
        }.getType();
        List<MMenu> listMenu = JSONUtil.DeSerialize(responseMenu, list);
        String response = NetworkUtils.getResponse(apiUrl + "items/" + id + "/" + page);
        JsonObject arr = JSONUtil.DeSerialize(response, JsonObject.class);
        Type listType = new TypeToken<List<MItem>>() {
        }.getType();
        List<MItem> listItem = JSONUtil.DeSerialize(arr.get("items").toString(), listType);
        model.addAttribute("static_url", ConfigInfo.STATIC_SERVER);
        model.addAttribute("itemList", listItem);
        model.addAttribute("menuList", listMenu);
        model.addAttribute("id", id);
        model.addAttribute("page", Integer.valueOf(arr.get("totalPages").toString()));
        model.addAttribute("nextPage", page + 1);
        model.addAttribute("prePage", page - 1);
        model.addAttribute("currentPage", Integer.valueOf(arr.get("page").toString()));
        return "item";
    }

    @RequestMapping("/content")
    public String content(Model model, @RequestParam(name = "id", defaultValue = "0") Long id) {
        String responseMenu = NetworkUtils.getResponse(apiUrl + "menus");
        Type list = new TypeToken<List<MMenu>>() {
        }.getType();
        List<MMenu> listMenu = JSONUtil.DeSerialize(responseMenu, list);
        String response = NetworkUtils.getResponse(apiUrl + "newitems");
        Type listType = new TypeToken<List<MItem>>() {
        }.getType();
        List<MItem> listNew = JSONUtil.DeSerialize(response, listType);
        String responseitem = NetworkUtils.getResponse(apiUrl + "item/" + id);
        JsonObject arr = JSONUtil.DeSerialize(responseitem, JsonObject.class);
        MItem item = new MItem();
        item.setId(Long.valueOf(arr.get("id").getAsString()));
        item.setImgUrl(arr.get("imgUrl").getAsString());
        item.setTitle(arr.get("title").getAsString());
        item.setDescription(arr.get("description").getAsString());
        item.setContent(arr.get("content").getAsString());
        model.addAttribute("id",id);
        model.addAttribute("static_url", ConfigInfo.STATIC_SERVER);
        model.addAttribute("itemList", listNew);
        model.addAttribute("item", item);
        model.addAttribute("menuList", listMenu);
        return "content";
    }

    @RequestMapping("/contact")
    public String contact(Model model) {
        String responseMenu = NetworkUtils.getResponse(apiUrl + "menus");
        Type list = new TypeToken<List<MMenu>>() {
        }.getType();
        List<MMenu> listMenu = JSONUtil.DeSerialize(responseMenu, list);
        model.addAttribute("menuList", listMenu);
        return "contact";
    }

    @RequestMapping("menuItems")
    public String menuItems(Model model, @RequestParam(name = "id", defaultValue = "0") String id,
                            @RequestParam(name = "page", defaultValue = "1") int page) {
        String responseMenu = NetworkUtils.getResponse(apiUrl + "menus");
        Type list = new TypeToken<List<MMenu>>() {
        }.getType();
        List<MMenu> listMenu = JSONUtil.DeSerialize(responseMenu, list);
        String response = NetworkUtils.getResponse(apiUrl + "menuitems/" + id + "/" + page);
        JsonObject arr = JSONUtil.DeSerialize(response, JsonObject.class);
        Type listType = new TypeToken<List<MItem>>() {
        }.getType();
        List<MItem> listItem = JSONUtil.DeSerialize(arr.get("items").toString(), listType);
        model.addAttribute("static_url", ConfigInfo.STATIC_SERVER);
        model.addAttribute("itemList", listItem);
        model.addAttribute("menuList", listMenu);
        model.addAttribute("id", id);
        model.addAttribute("page", Integer.valueOf(arr.get("totalPages").toString()));
        model.addAttribute("nextPage", page + 1);
        model.addAttribute("prePage", page - 1);
        model.addAttribute("currentPage", Integer.valueOf(arr.get("page").toString()));
        return "menuitem";
    }

    @PostMapping("/addMail")
    public void addMail(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String email = request.getParameter("email");
        try {

            NetworkUtils.post(apiUrl + "emails/", URLEncoder.encode(email, "UTF-8"));

        } catch (Exception e) {
            e.printStackTrace();
        }
        response.sendRedirect("/");

    }
}
