package com.teso.puzzle.landing.main;

import com.teso.puzzle.landing.config.ConfigInfo;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication(scanBasePackages = {"com.teso"})
@EnableJpaAuditing
@EntityScan(basePackages = "com.teso")
@EnableJpaRepositories("com.teso")

public class WebServerDaemon {

    private static final String TAG = WebServerDaemon.class.getName();

    public static void main(String[] args) {
        System.err.println("==============>" + ConfigInfo.SPRING_BOOT_CONFIG);
        ConfigurableApplicationContext applicationContext = new SpringApplicationBuilder(WebServerDaemon.class)
                .properties("spring.config.location:" + ConfigInfo.SPRING_BOOT_CONFIG)
                .build().run(args);
    }
}
