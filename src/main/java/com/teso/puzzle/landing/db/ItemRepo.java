package com.teso.puzzle.landing.db;

import com.teso.puzzle.landing.ent.Item;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import java.util.List;

public interface ItemRepo extends JpaRepository<Item, Long> {
    public List<Item> findItemByProjectId(@Param("project_id") String projectId);

}
