package com.teso.puzzle.landing.db;

import com.teso.puzzle.landing.ent.Project;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProjectRepo extends JpaRepository<Project, String> {
}
