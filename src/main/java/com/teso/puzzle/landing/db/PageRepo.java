package com.teso.puzzle.landing.db;

import com.teso.puzzle.landing.ent.Item;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PageRepo extends JpaRepository<Item, Integer> {
    Page<Item> findAll(Pageable pageable);
}
