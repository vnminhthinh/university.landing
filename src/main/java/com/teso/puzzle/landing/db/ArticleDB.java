///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package com.teso.puzzle.landing.db;
//
//import com.teso.puzzle.landing.ent.Article;
//import java.util.List;
//import org.springframework.data.domain.Pageable;
//import org.springframework.data.jpa.repository.JpaRepository;
//import org.springframework.data.jpa.repository.Query;
//import org.springframework.data.repository.query.Param;
//import org.springframework.stereotype.Repository;
//
///**
// *
// * @author Huy Nguyen
// */
//@Repository
//public interface ArticleDB extends JpaRepository<Article, Integer>{
//
//    /**
//     *
//     * @param articleCateId
//     * @param active
//     * @param visible
//     * @param page
//     * @return
//     */
//    @Query("SELECT p FROM Article p WHERE active = :active AND visible = :visible AND articleCategoryId = :articleCateId order by published_time desc")
//    public List<Article> findArticle(@Param("articleCateId") Integer articleCateId, @Param("active") Short active, @Param("visible") Short visible, Pageable page);
//}
