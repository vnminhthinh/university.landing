/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.teso.puzzle.landing.ent;

/**
 *
 * @author Huy Nguyen
 */
public class ProjectInfo {
    private String id;
    private String name;
    private String thumnail;
    private int numImg;

    public ProjectInfo(String id, String name, String thumnail, int numImg){
        this.id = id;
        this.name = name;
        this.thumnail = thumnail;
        this.numImg = numImg;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getNumImg() {
        return numImg;
    }

    public String getThumnail() {
        return thumnail;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setNumImg(int numImg) {
        this.numImg = numImg;
    }

    public void setThumnail(String thumnail) {
        this.thumnail = thumnail;
    }
}
