/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.teso.puzzle.landing.ent;

/**
 *
 * @author Huy Nguyen
 */
public class ImageInfo {
    private String url;
    private String desc;
    private String title;

    public ImageInfo(String url, String title, String desc){
        this.url = url;
        this.desc = desc;
        this.title = title;
    }

    public ImageInfo(String url, String title){
        this.url = url;
        this.desc = title;
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDesc() {
        return desc;
    }

    public String getUrl() {
        return url;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public void setUrl(String url) {
        this.url = url;
    }


}
