package com.teso.puzzle.landing.Service;

import com.teso.puzzle.landing.db.PageRepo;
import com.teso.puzzle.landing.ent.Item;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import java.util.List;

public class PageService {
    @Autowired
    PageRepo recordRepository;
    public List<Item> findAll(PageRequest pageRequest){
        Page<Item> recordsPage = recordRepository.findAll(pageRequest);
        return recordsPage.getContent();
    }
}
